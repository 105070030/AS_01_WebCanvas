class menu {
    constructor(){
        this.fillcolor;
        this.strokecolor;
        this.color;
        this.size;
        this.fontsize;
        this.fontfam;
    }
}

var myMenu;
var slider;
var fontslider;
var brushValue;
var fontValue;
var type_select;
var colorPicker;
var isShow = true;

var hueCursors;
var spectrumCursor;

function init_menu()
{
    slider = document.getElementById("brushSize");
    brushValue =document.getElementById('brushValue');
    fontslider = document.getElementById("fontSize");
    fontValue =document.getElementById('fontValue');
    myMenu = new menu();

    slider.oninput = function() {
        myMenu.size = this.value;
        brushValue.value = this.value;
    };

    fontslider.oninput = function() {
        fontValue.value = this.value;
        myMenu.fontsize = this.value + 'px';
        ctx.font = myMenu.fontsize + ' ' + myMenu.fontfam;
        addText.style['font-size'] = myMenu.fontsize;
        addText.focus();
    };

    brushValue.addEventListener('change', function(){
        myMenu.size = this.value;
        slider.value = this.value;
    });

    fontValue.addEventListener('change', function(){
        myMenu.fontsize = this.value + 'px';
        fontslider.value = this.value;
        ctx.font = myMenu.fontsize + ' ' + myMenu.fontfam;
        addText.style['font-size'] = myMenu.fontsize;
        addText.focus();
    });
    color();
}

function color_panel(type)
{
    console.log(type.id);
    type_select.style['border']= 'solid 1px #dfdfdf';
    if(isShow && type.id == type_select.id){
        console.log('close panel');
        colorPicker.style['opacity']= '0';
        isShow = false;
    }else{
        type_select = document.getElementById(type.id);
        type_select.style['border']= 'solid 1px #00cdff';
        console.log('open panel');
        colorPicker.style['opacity']= '1';
        isShow = true;
    }
}

function color()
{
    var colorBlock = document.getElementById('color-block');
    var ctx1 = colorBlock.getContext('2d');
    var width1 = colorBlock.width;
    var height1 = colorBlock.height;

    var colorStrip = document.getElementById('color-strip');
    var ctx2 = colorStrip.getContext('2d');
    var width2 = colorStrip.width;
    var height2 = colorStrip.height;

    colorPicker = document.getElementById('color-picker');
    type_select = document.getElementById('fill-color');

    hueCursor = document.getElementById('hue-cursor');
    spectrumCursor = document.getElementById('spectrum-cursor');

    var x = 0;
    var y = 0;
    var drag = false;
    myMenu.fillcolor = 'rgba(0,0,0,1)';
    myMenu.strokecolor = 'rgba(255,255,255,1)';
    type_select.style.backgroundcolor = 'rgba(0,0,0,1)';

    ctx1.rect(0, 0, width1, height1);
    fillGradient();

    ctx2.rect(0, 0, width2, height2);
    var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
    grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
    grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
    grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
    grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
    grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
    grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
    ctx2.fillStyle = grd1;
    ctx2.fill();

    function click(e) {
    x = e.offsetX;
    y = e.offsetY;
    hueCursor.style.top = y - 3 + 'px';
    console.log(hueCursor.style.top);
    var imageData = ctx2.getImageData(x, y, 1, 1).data;
    myMenu.color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    fillGradient();
    }

    function fillGradient() {
    ctx1.fillStyle = myMenu.color;
    ctx1.fillRect(0, 0, width1, height1);

    var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    ctx1.fillStyle = grdWhite;
    ctx1.fillRect(0, 0, width1, height1);

    var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    ctx1.fillStyle = grdBlack;
    ctx1.fillRect(0, 0, width1, height1);
    }

    function mousedown(e) {
    drag = true;
    x = e.offsetX;
    y = e.offsetY;
    spectrumCursor.style.left = x - 11 + 'px';
    spectrumCursor.style.top = y - 10 + 'px';
    changeColor(e);
    }

    function mousemove(e) {
    if (drag) {
        changeColor(e);
    }
    }

    function mouseup(e) {
        drag = false;
        if(type_select.id == 'fill-color'){
            myMenu.fillcolor = type_select.style.backgroundColor;
        }else if(type_select.id == 'stroke-color'){
            myMenu.strokecolor = type_select.style.backgroundColor;
        }
    }

    function changeColor(e) {
    x = e.offsetX;
    y = e.offsetY;
    spectrumCursor.style.left = x - 11 + 'px';
    spectrumCursor.style.top = y - 10 + 'px';
    var imageData = ctx1.getImageData(x, y, 1, 1).data;
    type_select.style.backgroundColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    }

    colorStrip.addEventListener("click", click, false);

    colorBlock.addEventListener("mousedown", mousedown, false);
    colorBlock.addEventListener("mouseup", mouseup, false);
    colorBlock.addEventListener("mousemove", mousemove, false);
    colorBlock.addEventListener('mouseout', () => drag = false);
}

window.onload = function () {
    init_menu();
}