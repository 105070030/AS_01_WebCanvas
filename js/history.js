class history {
    constructor() {
        this.redo_list = [];
        this.undo_list = [];
        self = this;
    }

    saveState(ToPush) {
        ToPush.push(canvas.toDataURL());
        if(ToPush.length>=5){
            console.log('shift older record');
            ToPush.shift();
        }else {
            console.log('push to history');
        }
    }

    restoreState(toPop, toPush) {
        if(toPop.length) {
            self.saveState(toPush);
            var restore_URL = toPop.pop();

            var img = new Image();
            img.src = restore_URL;
            img.onload = function(){
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img,0,0,canvas.width,canvas.height,0,0,canvas.width,canvas.height);
            };
        }
    }

    undo() {
        self.restoreState(self.undo_list, self.redo_list);
    }

    redo() {
        self.restoreState(self.redo_list, self.undo_list);
    }
}