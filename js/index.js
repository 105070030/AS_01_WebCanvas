var canvas;
var tmpcanvas;
var tool = 1;
var addText;

var isDrawing = false;
var ctx;
var tmpctx;
var [lastX, lastY] = [0, 0];
var [currX, currY] = [0, 0]

function pickTool(selection) {
    switch (selection.id) {
        case 'clear':
            console.log(selection.id);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
            break;
        case 'brush':
            console.log(selection.id);
            tool = 0;
            tmpcanvas.setAttribute("style","cursor: url('cursor/brush.cur'), pointer;");
            break;
        case 'square':
            console.log(selection.id);
            tool = 1;
            tmpcanvas.setAttribute("style","cursor: url('cursor/crop_rec.cur'), pointer;");
            break;
        case 'triangle':
            console.log(selection.id);
            tool = 2;
            tmpcanvas.setAttribute("style","cursor: url('cursor/crop_tri.cur'), pointer;");
            break;
        case 'line':
            console.log(selection.id);
            tool = 3;
            tmpcanvas.setAttribute("style","cursor: url('cursor/crop_line.cur'), pointer;");
            break;
        case 'circle':
            console.log(selection.id);
            tool = 4;
            tmpcanvas.setAttribute("style","cursor: url('cursor/crop_cir.cur'), pointer;");
            break;
        case 'text':
            console.log(selection.id);
            tool = 5;
            tmpcanvas.setAttribute("style","cursor: text;");
            break;
        case 'eraser':
            console.log(selection.id);
            tool = 6;
            tmpcanvas.setAttribute("style","cursor: url('cursor/eraser.cur'), pointer;");
            break;
        case 'upload':
            fileUpload.click();
            break;
    }
}

function draw(e) {

    if (!isDrawing) return;
    // [currX, currY] = [e.pageX, e.pageY];
    let [startposX, startposY] = [Math.min(lastX, e.offsetX), Math.min(lastY, e.offsetY)];
    switch (tool) {
        case 0: //brush
            ctx.beginPath();
            ctx.moveTo(lastX, lastY)
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();

            [lastX, lastY] = [e.offsetX, e.offsetY];
            break;
        case 1: //rectangle
            tmpctx.beginPath();
            tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
            tmpctx.rect(startposX, startposY, Math.abs(e.offsetX - lastX), Math.abs(e.offsetY - lastY));
            tmpctx.fill();
            tmpctx.stroke();
            break;
        case 2: //triangle
            tmpctx.beginPath();
            tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
            tmpctx.beginPath();
            tmpctx.moveTo(lastX, e.offsetY);
            tmpctx.lineTo(e.offsetX, e.offsetY);
            tmpctx.lineTo((lastX + e.offsetX) / 2, lastY);
            tmpctx.closePath();
            tmpctx.fill();
            tmpctx.stroke();

            tmpctx.save();

            tmpctx.setLineDash([1,1]);
            tmpctx.strokeStyle = "#dfdfdf";
            tmpctx.lineWidth = 1;
            tmpctx.beginPath();
            tmpctx.rect(startposX, startposY, Math.abs(e.offsetX-lastX), Math.abs(e.offsetY-lastY));
            tmpctx.stroke();

            tmpctx.restore();
            break;
        case 3: //line
            tmpctx.beginPath();
            tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
            tmpctx.moveTo(lastX, lastY);
            tmpctx.lineTo(e.offsetX, e.offsetY);
            tmpctx.stroke();
            break;
        case 4: //circle
            tmpctx.beginPath();    
            tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
            let radius = Math.min(Math.abs(e.offsetX-lastX)/2, Math.abs(e.offsetY-lastY)/2);
            let [centerX, centerY] = [lastX + radius * Math.sign(e.offsetX-lastX), lastY + radius * Math.sign(e.offsetY-lastY)];
            
            tmpctx.beginPath();
            tmpctx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
            tmpctx.fill();
            tmpctx.stroke();

            tmpctx.save();

            tmpctx.setLineDash([1,1]);
            tmpctx.strokeStyle = "#dfdfdf";
            tmpctx.lineWidth = 1;
            [startposX, startposY] = [centerX - radius, centerY - radius];
            tmpctx.beginPath();
            tmpctx.rect(startposX, startposY, 2 * radius, 2 * radius);
            tmpctx.stroke();
            tmpctx.restore();
            break;
        case 5: //text

            break;
        case 6: //eraser
            ctx.globalCompositeOperation = 'destination-out';
            ctx.beginPath();
            ctx.moveTo(lastX, lastY)
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();

            [lastX, lastY] = [e.offsetX, e.offsetY];
            break;
    }

}

function tempDone(e) {
    isDrawing = false;
    tmpctx.clearRect(0, 0, tmpcanvas.width, tmpcanvas.height);
    switch (tool) {
        case 1: //rectangle
            if (e.offsetX >= lastX && e.offsetY >= lastY) {
                ctx.beginPath();
                ctx.rect(lastX, lastY, e.offsetX - lastX, e.offsetY - lastY);
                ctx.fill();
                ctx.stroke();
            } else {
                ctx.beginPath();
                ctx.rect(e.offsetX, e.offsetY, lastX - e.offsetX, lastY - e.offsetY);
                ctx.fill();
                ctx.stroke();
            }
            break;
        case 2: //triangle
            ctx.beginPath();
            ctx.moveTo(lastX, e.offsetY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.lineTo((lastX + e.offsetX) / 2, lastY);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
            break;
        case 3: //line
            ctx.beginPath();
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
            break;
        case 4: //circle
            let radius = Math.min(Math.abs(e.offsetX-lastX)/2, Math.abs(e.offsetY-lastY)/2);
            let [centerX, centerY] = [lastX + radius * Math.sign(e.offsetX-lastX), lastY + radius * Math.sign(e.offsetY-lastY)];
            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
            break;
        case 5: //text
            addText.disabled = false;
            addText.style.display = 'block';
            addText.style.top = e.offsetY - 20 + 'px';
            addText.style.left = e.offsetX + 'px';
            addText.focus();
            break;
        case 6:
            ctx.globalCompositeOperation = 'source-over';
            break;
    }

}

function setFontFam(self) {
    myMenu.fontfam = self.value;
    ctx.font = myMenu.fontsize + ' ' + myMenu.fontfam;
    addText.style['font-family'] = self.value;
    addText.focus();
}

function editText(event) {
    if(event.keyCode == 13){
        addText.style.display = 'none';
        ctx.fillText(addText.value, lastX, lastY);
        addText.value = '';
        addText.disabled = true;
        addText.style.display = 'none';
    }
}

download_img = function(el) {
    var image = canvas.toDataURL("image/jpg");
    el.href = image;
};


function readImage() {
    if ( this.files && this.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
           var img = new Image();
           img.src = e.target.result;
           img.onload = function() {
             ctx.drawImage(img, 0, 0,img.width,img.height,0,0,800,600);
             console.log('draw');
           };
        };       
        FR.readAsDataURL( this.files[0] );
    }
}

function init() {

    var fileUpload = document.getElementById('fileUpload');
    fileUpload.onchange = readImage;

    canvas = document.getElementById("canvas");
    tmpcanvas = document.getElementById("tempcanvas");
    anime = document.getElementById("anime");
    ctx = canvas.getContext("2d");
    tmpctx = tmpcanvas.getContext("2d");

    myhistory = new history();

    tmpctx.strokeStyle = "#000000";
    tmpctx.save();

    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    

    tmpcanvas.addEventListener('mousedown', (e) => {
        isDrawing = true;
        myhistory.saveState(myhistory.undo_list);

        ctx.strokeStyle = myMenu.strokecolor;
        ctx.fillStyle = myMenu.fillcolor;
        tmpctx.strokeStyle = myMenu.strokecolor;
        tmpctx.fillStyle = myMenu.fillcolor;
        ctx.lineWidth = myMenu.size;
        tmpctx.lineWidth = myMenu.size;
        [lastX, lastY] = [e.offsetX, e.offsetY];
    });
    tmpcanvas.addEventListener('mousemove', draw);
    tmpcanvas.addEventListener('mouseup', tempDone);
    tmpcanvas.addEventListener('mouseout', () => isDrawing = false);

    document.getElementById('undo').addEventListener('click', myhistory.undo);
    document.getElementById('redo').addEventListener('click', myhistory.redo);

    addText = document.getElementById("add-text");
}

class menu {
    constructor(){
        this.fillcolor;
        this.strokecolor;
        this.color;
        this.size;
        this.fontsize = '10px';
        this.fontfam = 'Arial';
    }
}

var myMenu;
var slider;
var fontslider;
var brushValue;
var fontValue;
var type_select;
var colorPicker;
var isShow = true;

var hueCursors;
var spectrumCursor;

function init_menu()
{
    slider = document.getElementById("brushSize");
    brushValue =document.getElementById('brushValue');
    fontslider = document.getElementById("fontSize");
    fontValue =document.getElementById('fontValue');
    myMenu = new menu();

    slider.oninput = function() {
        myMenu.size = this.value;
        brushValue.value = this.value;
    };

    fontslider.oninput = function() {
        fontValue.value = this.value;
        myMenu.fontsize = this.value + 'px';
        ctx.font = myMenu.fontsize + ' ' + myMenu.fontfam;
        addText.style['font-size'] = myMenu.fontsize;
        addText.focus();
    };

    brushValue.addEventListener('change', function(){
        myMenu.size = this.value;
        slider.value = this.value;
    });

    fontValue.addEventListener('change', function(){
        myMenu.fontsize = this.value + 'px';
        fontslider.value = this.value;
        ctx.font = myMenu.fontsize + ' ' + myMenu.fontfam;
        addText.style['font-size'] = myMenu.fontsize;
        addText.focus();
    });
    color();
}

function color_panel(type)
{
    console.log(type.id);
    type_select.style['border']= 'solid 1px #dfdfdf';
    if(isShow && type.id == type_select.id){
        console.log('close panel');
        colorPicker.style['opacity']= '0';
        isShow = false;
    }else{
        type_select = document.getElementById(type.id);
        type_select.style['border']= 'solid 1px #00cdff';
        console.log('open panel');
        colorPicker.style['opacity']= '1';
        isShow = true;
    }
}

function color()
{
    var colorBlock = document.getElementById('color-block');
    var ctx1 = colorBlock.getContext('2d');
    var width1 = colorBlock.width;
    var height1 = colorBlock.height;

    var colorStrip = document.getElementById('color-strip');
    var ctx2 = colorStrip.getContext('2d');
    var width2 = colorStrip.width;
    var height2 = colorStrip.height;

    colorPicker = document.getElementById('color-picker');
    type_select = document.getElementById('fill-color');

    hueCursor = document.getElementById('hue-cursor');
    spectrumCursor = document.getElementById('spectrum-cursor');

    var x = 0;
    var y = 0;
    var drag = false;
    myMenu.fillcolor = 'rgba(0,0,0,1)';
    myMenu.strokecolor = 'rgba(255,255,255,1)';
    type_select.style.backgroundcolor = 'rgba(0,0,0,1)';

    ctx1.rect(0, 0, width1, height1);
    fillGradient();

    ctx2.rect(0, 0, width2, height2);
    var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
    grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
    grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
    grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
    grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
    grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
    grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
    ctx2.fillStyle = grd1;
    ctx2.fill();

    function click(e) {
    x = e.offsetX;
    y = e.offsetY;
    hueCursor.style.top = y - 3 + 'px';
    console.log(hueCursor.style.top);
    var imageData = ctx2.getImageData(x, y, 1, 1).data;
    myMenu.color = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    fillGradient();
    }

    function fillGradient() {
    ctx1.fillStyle = myMenu.color;
    ctx1.fillRect(0, 0, width1, height1);

    var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    ctx1.fillStyle = grdWhite;
    ctx1.fillRect(0, 0, width1, height1);

    var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
    grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
    ctx1.fillStyle = grdBlack;
    ctx1.fillRect(0, 0, width1, height1);
    }

    function mousedown(e) {
    drag = true;
    x = e.offsetX;
    y = e.offsetY;
    spectrumCursor.style.left = x - 11 + 'px';
    spectrumCursor.style.top = y - 10 + 'px';
    changeColor(e);
    }

    function mousemove(e) {
    if (drag) {
        changeColor(e);
    }
    }

    function mouseup(e) {
        drag = false;
        if(type_select.id == 'fill-color'){
            myMenu.fillcolor = type_select.style.backgroundColor;
        }else if(type_select.id == 'stroke-color'){
            myMenu.strokecolor = type_select.style.backgroundColor;
        }
    }

    function changeColor(e) {
    x = e.offsetX;
    y = e.offsetY;
    spectrumCursor.style.left = x - 11 + 'px';
    spectrumCursor.style.top = y - 10 + 'px';
    var imageData = ctx1.getImageData(x, y, 1, 1).data;
    type_select.style.backgroundColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    }

    colorStrip.addEventListener("click", click, false);

    colorBlock.addEventListener("mousedown", mousedown, false);
    colorBlock.addEventListener("mouseup", mouseup, false);
    colorBlock.addEventListener("mousemove", mousemove, false);
    colorBlock.addEventListener('mouseout', () => drag = false);
}




window.onload = function () {
    init();
    init_menu();
}